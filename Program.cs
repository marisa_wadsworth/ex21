﻿using System;

namespace ex21
{
    class Program
    {
        static void Main(string[] args)
        {
            /// Task A ///
            
            int count = 5;
            int x;

            for(x = 0; x < count; x++)
            {
                Console.WriteLine($"Numbers : {x+1} ");
            }

            Console.WriteLine("");
            Console.WriteLine("");

            /// Task B ///
            
            x = 0;
            while(x < count)
            {
                Console.WriteLine($"Numbers : {x+1}");

                ++x;
            }
            
            Console.WriteLine("");
            Console.WriteLine("");

             /// Task C ///

             count = 20;
             for(x=1; x<= count; x++)
             {
                if(x%2 == 0)
                {
                    Console.WriteLine($"Numbers: {x}");
                }
                
                x = 1;
                while(x <= count)
                {
                    if(x%2 ==0)
                    {
                        Console.WriteLine($"Numbers : {x}");
                    }

                    ++x;
                }

            Console.WriteLine("");
            Console.WriteLine("");

                /// Task D ///

                x = 10;
                count = 5;
                do
                {
                    Console.WriteLine($"Index : {x}");
                    Console.WriteLine($"Counter : {count}");
                    if(x == count)
                    {
                        Console.WriteLine($"Index({x}) is the same as the counter ({count})");
                    }
                    else if(x>count)
                    {
                        Console.WriteLine($"Index ({x}) is greater than the counter ({count})");
                    }
                    else
                    {
                        Console.WriteLine("Something is wrong");
                    }
                } while(x<5);
             }
        }
    }
}